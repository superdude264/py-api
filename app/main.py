from logging import Logger

import structlog
from fastapi import FastAPI
from opentelemetry import metrics, trace

from app.config import get_settings
from app.telemetry import setup_telemetry

settings = get_settings()
setup_telemetry(settings)
log: Logger = structlog.get_logger(project=settings.project_name)
tracer = trace.get_tracer(settings.project_name)
meter = metrics.get_meter(settings.project_name)
svc = FastAPI(title=settings.project_name)

greeting_counter = meter.create_counter(f"{settings.project_name}.greet.count")


@svc.get("/")
def read_root():
    return {"Hello": "World"}


@svc.get("/greet/{name}")
def greet(name: str):
    with tracer.start_as_current_span("greet"):
        log.info(f"Preparing to greet: {name}")
        greeting_counter.add(1)
        return {"Hello": name}
