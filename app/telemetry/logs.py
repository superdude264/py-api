# type: ignore
import logging
from enum import Enum
from logging import Formatter, Handler, Logger, StreamHandler

import structlog
from opentelemetry import trace
from opentelemetry.sdk.resources import Resource
from structlog.contextvars import bind_contextvars, clear_contextvars
from structlog.stdlib import ProcessorFormatter

from app.config import Settings


def add_trace_info(_, __, event_dict):
    # https://www.structlog.org/en/stable/frameworks.html#opentelemetry
    span = trace.get_current_span()
    if not span.is_recording():
        event_dict["span"] = None
        return event_dict

    ctx = span.get_span_context()
    parent = getattr(span, "parent", None)
    event_dict["span"] = {
        "span_id": hex(ctx.span_id),
        "trace_id": hex(ctx.trace_id),
        "parent_span_id": None if not parent else hex(parent.span_id),
    }
    return event_dict


shared_processors = [
    # If log level is too low, abort pipeline and throw away log entry.
    structlog.stdlib.filter_by_level,
    # Add trace info.
    add_trace_info,
    # Add context variables.
    structlog.contextvars.merge_contextvars,
    # Add the name of the logger to event dict.
    structlog.stdlib.add_logger_name,
    # Add log level to event dict.
    structlog.stdlib.add_log_level,
    # Perform %-style formatting.
    structlog.stdlib.PositionalArgumentsFormatter(),
    # Add a timestamp in ISO 8601 format.
    structlog.processors.TimeStamper(fmt="iso"),
    # If the "stack_info" key in the event dict is true, remove it and
    # render the current stack trace in the "stack" key.
    structlog.processors.StackInfoRenderer(),
    # If some value is in bytes, decode it to a unicode str.
    structlog.processors.UnicodeDecoder(),
    # Add call site parameters.
    structlog.processors.CallsiteParameterAdder(
        {
            structlog.processors.CallsiteParameter.MODULE,
            structlog.processors.CallsiteParameter.FUNC_NAME,
            structlog.processors.CallsiteParameter.LINENO,
        },
    ),
]

json_formatter = ProcessorFormatter(
    # These run ONLY on `logging` entries that do NOT originate within
    # structlog.
    foreign_pre_chain=shared_processors,
    # These run on ALL entries after the pre_chain is done. Order matters.
    processors=[
        # Remove _record & _from_structlog.
        ProcessorFormatter.remove_processors_meta,
        # Log JSON
        structlog.processors.dict_tracebacks,
        structlog.processors.format_exc_info,
        structlog.processors.JSONRenderer(),
    ],
)

console_formatter = ProcessorFormatter(
    # These run ONLY on `logging` entries that do NOT originate within
    # structlog.
    foreign_pre_chain=shared_processors,
    # These run on ALL entries after the pre_chain is done. Order matters.
    processors=[
        # Remove _record & _from_structlog.
        ProcessorFormatter.remove_processors_meta,
        # Log to console
        structlog.dev.ConsoleRenderer(),
    ],
)


class LogFormat(Enum):
    JSON = json_formatter
    CONSOLE = console_formatter


def setup_python_logging(logger: Logger, handler: Handler, formatter: Formatter, level: str):
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(level)


def setup_struct_logging(logger: Logger, handler: Handler, formatter: ProcessorFormatter, level: str):
    setup_python_logging(logger=logger, handler=handler, formatter=formatter, level=level)
    formatter.logger = logger
    structlog.configure(
        processors=[*shared_processors, structlog.stdlib.ProcessorFormatter.wrap_for_formatter],
        logger_factory=structlog.stdlib.LoggerFactory(),
        # wrapper_class=structlog.stdlib.BoundLogger,
        cache_logger_on_first_use=True,
    )


def setup_logs(resource: Resource, settings: Settings):
    setup_struct_logging(
        logger=logging.root,
        handler=StreamHandler(),
        formatter=LogFormat[settings.logging.format].value,
        level=settings.logging.level,
    )
    clear_contextvars()
    bind_contextvars(**resource.attributes)
