from app.config import Settings


def setup_telemetry(settings: Settings):
    from opentelemetry.sdk.resources import Resource, ResourceAttributes

    from app.telemetry.logs import setup_logs  # type: ignore
    from app.telemetry.metrics import setup_metrics
    from app.telemetry.trace import setup_trace

    # TODO Make configurable.
    res = Resource(
        attributes={
            ResourceAttributes.SERVICE_NAME: settings.project_name,
            ResourceAttributes.DEPLOYMENT_ENVIRONMENT: settings.environment,
        },
    )
    setup_logs(res, settings)
    setup_trace(res)
    setup_metrics(res)
