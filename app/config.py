from functools import lru_cache
from typing import Literal

from pydantic import BaseSettings


class LoggingSettings(BaseSettings):
    level: Literal[
        "CRITICAL",
        "FATAL",
        "ERROR",
        "WARN",
        "WARNING",
        "INFO",
        "METRIC",
        "DEBUG",
        "TRACE",
    ]
    format: Literal["JSON", "CONSOLE"]  # noqa: A003


class Settings(BaseSettings):
    project_name: str
    environment: str
    logging: LoggingSettings

    class Config:
        env_prefix = ""
        env_nested_delimiter = "."
        env_file = ".env"
        env_file_encoding = "utf-8"


@lru_cache()
def get_settings():
    return Settings()
