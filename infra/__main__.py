import pulumi
from pulumi_gcp import cloudrun

# Import the provider's configuration settings.
gcp_config = pulumi.Config("gcp")
location = gcp_config.require("region")
project = gcp_config.require("project")

# Import the program's configuration settings.
config = pulumi.Config()
container_port = config.get_int("containerPort", 8080)
cpu = config.require("cpu")
memory = config.require("memory")
concurrency = config.require_int("concurrency")

project_name = "py-api"
# TODO: Create svc account in stack.
service_account_name = "114837862152-compute@developer.gserviceaccount.com"

# Create a Cloud Run service definition.
service = cloudrun.Service(
    project_name,
    cloudrun.ServiceArgs(
        location=location,
        template=cloudrun.ServiceTemplateArgs(
            spec=cloudrun.ServiceTemplateSpecArgs(
                container_concurrency=concurrency,
                timeout_seconds=300,
                service_account_name=service_account_name,
                containers=[
                    cloudrun.ServiceTemplateSpecContainerArgs(
                        image=f"{location}-docker.pkg.dev/{project}/images/{project_name}:latest",
                        resources=cloudrun.ServiceTemplateSpecContainerResourcesArgs(
                            limits=dict(
                                memory=memory,
                                cpu=cpu,
                            ),
                        ),
                        ports=[
                            cloudrun.ServiceTemplateSpecContainerPortArgs(
                                container_port=container_port,
                            ),
                        ],
                        startup_probe=cloudrun.ServiceTemplateSpecContainerStartupProbeArgs(
                            timeout_seconds=240,
                            period_seconds=240,
                            failure_threshold=1,
                            tcp_socket=cloudrun.ServiceTemplateSpecContainerStartupProbeTcpSocketArgs(
                                port=container_port
                            ),
                        ),
                    ),
                ],
            ),
            metadata=cloudrun.ServiceTemplateMetadataArgs(annotations={"autoscaling.knative.dev/maxScale": "1"}),
        ),
        traffics=[cloudrun.ServiceTrafficArgs(percent=100, latest_revision=True)],
    ),
)

# Create an IAM member to make the service publicly accessible.
invoker = cloudrun.IamMember(
    "invoker",
    cloudrun.IamMemberArgs(
        location=location,
        service=service.name,
        role="roles/run.invoker",
        member="allUsers",
    ),
)
