SHELL := bash
.ONESHELL:
.SHELLFLAGS := -euo pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

.DEFAULT_GOAL := help

### Local Dev ###

# Alias a fragment of the docker compose command to use when working with the devcontainer ('devcon') image target.
dc-devcon := docker compose -f docker-compose.yml -f docker-compose.devcon.yml

devcon: ## Build the devcontainer for use in local development.
	$(dc-devcon) build app
.PHONY: devcon

devcon-up:  ## Start the app in debug mode with in the devcontainer.
	$(dc-devcon) up --build app && \
	$(dc-devcon) down --remove-orphans
.PHONY: devcon-up

devcon-shell: ## Open a shell in the devcontainer.
	$(dc-devcon) run --build --interactive --rm --entrypoint /bin/bash app && \
	$(dc-devcon) down --remove-orphans
.PHONY: devcon-shell

format: ## Format all Python code using black.
	black .
.PHONY: format

lint: ## Lint all Python code using ruff, fixing issues where possible.
	ruff check --fix .
.PHONY: lint

fix: format lint ## Format and lint, fixing issues where possible.
.PHONY: fix

type-check: ## Type check Python code with mypy.
	mypy .
.PHONY: type-check

check: ## Check for formatting, linting, and typing violations.
	black --check .
	ruff check .
	mypy .
.PHONY: check

### CI ###

dist: ## Build the distribution image.
	docker compose build app
.PHONY: dist # Note that building the dist image performs all the format, lint, & type checks.

push: dist ## Build & push the distribution image. Use this for CI.
	docker compose push app
.PHONY: push

### Deployment ###

preview-dev: ## Preview infrastructure changes to DEV.
	pulumi preview --stack dev --diff
.PHONY: preview-dev

deploy-dev: push ## Build, push, & deploy the distribution image along with any infrastructure changes to DEV.
	pulumi up --stack dev --diff --non-interactive --yes
.PHONY: infra-preview-dev

### Misc ###
help: ## Print target help messages.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
.PHONY: help # See https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
