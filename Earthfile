VERSION 0.7
ARG --global PROJECT_NAME
ARG --global IMAGE_REPO
ARG --global PYTHON_VERSION

### Local Dev ###

devcon:
    FROM --platform=linux/amd64 python:${PYTHON_VERSION}-bullseye
    ARG POETRY_VERSION
    ARG PULUMI_VERSION
    ENV DEBIAN_FRONTEND=noninteractive
    ENV PYTHONUNBUFFERED=1
    ENV PIP_NO_CACHE_DIR=1
    ENV PIP_DISABLE_PIP_VERSION_CHECK=1
    ENV POETRY_VIRTUALENVS_CREATE=0
    WORKDIR /opt
    RUN apt-get update
    RUN wget --quiet -O- https://get.pulumi.com/releases/sdk/pulumi-v${PULUMI_VERSION}-linux-x64.tar.gz | \
        tar xz -C /usr/local/bin --strip-components=1 && \
        pulumi version
    RUN wget --quiet -O- https://install.python-poetry.org | \
        POETRY_VERSION=$POETRY_VERSION python - && \
        cp /root/.local/bin/poetry /usr/local/bin/ && \
        poetry --version
    COPY poetry.lock pyproject.toml ./
    RUN poetry install --no-interaction --no-root --no-cache
    COPY Pulumi.yaml ./
    COPY infra infra
    COPY app app
    SAVE IMAGE $IMAGE_REPO-dev:latest

format:
    FROM +devcon
    DO +BLACK

lint:
    FROM +devcon
    DO +RUFF

fix:
    FROM +devcon
    BUILD +format
    BUILD +lint

type-check:
    FROM +devcon
    DO +MYPY

check:
    FROM +devcon
    DO +BLACK --fix=false
    DO +RUFF --fix=false
    DO +MYPY

build:
    FROM +devcon
    BUILD +check
    RUN poetry build --format=wheel --no-cache
    SAVE ARTIFACT dist/app-0.0.0-py3-none-any.whl

#### CI ###

dist:
    FROM --platform=linux/amd64 python:${PYTHON_VERSION}-slim-bullseye
    ENV DEBIAN_FRONTEND=noninteractive
    ENV PYTHONUNBUFFERED=1
    ENV PIP_NO_CACHE_DIR=1
    ENV PIP_DISABLE_PIP_VERSION_CHECK=1
    RUN apt-get update && \
        apt-get install -y --no-install-recommends \
            ca-certificates \
            netbase \
            tzdata \
        && \
        apt-get clean && \
        rm -rf /var/lib/apt/lists/*
    WORKDIR /opt
    COPY +build/app-0.0.0-py3-none-any.whl .
    RUN pip install app-0.0.0-py3-none-any.whl
    USER nobody
    ENTRYPOINT ["uvicorn", "--host", "0.0.0.0", "--port", "8080", "app.main:svc"]
    SAVE IMAGE --push $IMAGE_REPO:latest


#### Deployment ###

# preview-dev:
#     FROM +devcon
#     RUN preview --stack dev --diff
#
# deploy-dev:
#     FROM +devcon
#     BUILD +dist
#     RUN --push pulumi up --stack dev --diff --non-interactive --yes

### Commands ###

BLACK:
    COMMAND
    ARG fix=true
    IF $fix
        RUN black .
        SAVE ARTIFACT app AS LOCAL app
    ELSE
        RUN black --check .
    END

RUFF:
    COMMAND
    ARG fix=true
    CACHE .ruff_cache
    IF $fix
        RUN ruff check --fix .
        SAVE ARTIFACT app AS LOCAL app
    ELSE
        RUN ruff check .
    END
    SAVE ARTIFACT .ruff_cache AS LOCAL .ruff_cache

MYPY:
    # Slow Docker for Mac due to poor file I/O speed.
    COMMAND
    CACHE .mypy_cache
    RUN mypy .
    SAVE ARTIFACT .mypy_cache AS LOCAL .mypy_cache

