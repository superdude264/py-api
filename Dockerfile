# syntax=docker/dockerfile:1
ARG PYTHON_VERSION

FROM --platform=linux/amd64 python:${PYTHON_VERSION}-bullseye AS devcon
ARG POETRY_VERSION
ARG PULUMI_VERSION
ENV DEBIAN_FRONTEND=noninteractive
ENV PYTHONUNBUFFERED=1
ENV PIP_NO_CACHE_DIR=1
ENV PIP_DISABLE_PIP_VERSION_CHECK=1
ENV POETRY_VIRTUALENVS_CREATE=0
WORKDIR /opt

# Update package list to allow ad-hoc usage of apt-get in devcontainer.
# If any Python packages need to be compiled, see: https://pythonspeed.com/articles/multi-stage-docker-python/
RUN apt-get update

### Pulumi ###
# Download the tarball to stdout (-O-), pipe to tar where the files under the unarchived parent directory
# (strip-components=1, e.g. 1/2/3) are written to /user/local/bin, which is already on $PATH.
RUN wget --quiet -O- https://get.pulumi.com/releases/sdk/pulumi-v${PULUMI_VERSION}-linux-x64.tar.gz | \
    tar xz -C /usr/local/bin --strip-components=1 && \
    pulumi version

### Poetry ###
RUN wget --quiet -O- https://install.python-poetry.org | \
    POETRY_VERSION=$POETRY_VERSION python - && \
    cp /root/.local/bin/poetry /usr/local/bin/ && \
    poetry --version

### Python Dependencies ###
COPY poetry.lock pyproject.toml ./
RUN poetry install --no-interaction --no-root --no-cache

### Code ###
COPY Pulumi.yaml ./
COPY infra infra
COPY app app


FROM devcon AS build
RUN black --check . && \
    ruff check . && \
    mypy . && \
    poetry build --format=wheel --no-cache


FROM --platform=linux/amd64 python:${PYTHON_VERSION}-slim-bullseye AS dist
ENV DEBIAN_FRONTEND=noninteractive
ENV PYTHONUNBUFFERED=1
ENV PIP_NO_CACHE_DIR=1
ENV PIP_DISABLE_PIP_VERSION_CHECK=1
WORKDIR /opt
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        ca-certificates \
        netbase \
        tzdata \
    && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*
COPY --from=build /opt/dist/app-0.0.0-py3-none-any.whl .
RUN pip install app-0.0.0-py3-none-any.whl
USER nobody
ENTRYPOINT ["uvicorn", "--host", "0.0.0.0", "--port", "8080", "app.main:svc"]
